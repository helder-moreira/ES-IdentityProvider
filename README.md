ServiceEngineeringDETI

__To run this project you need:__
* Python environment
* SQLite3 installed
* Flask framework and the following extensions, which if you have pip installed you can install with:
	* sudo pip install Flask rauth flask-sqlalchemy flask-login flask-oauthlib 

__How to run:__
* Set configurations on config.py
* Run the python script
	* python app.py
