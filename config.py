import os

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

#Folder to store uploaded profile photos
UPLOAD_FOLDER = '/static/uploaded'

########## Web interface ###############
#Session time
session_time = 100
#Set if a user must always be remembered
remember_me = False
########################################

########## REST API ####################
#Expiration time for uuids generated for
#login with external accounts
uuid_time = 60
#Expiration of access tokens
token_time = 600
#Expiration of app passwords
app_passwords_time = 1000
#APP passwords length
temp_passwords_length = 32
########################################

######### OAuth server #################
#Expiration of a grant access by oauth server
grant_time = 300
########################################

######### Server configs ###############
debug=True
server_port = 8080
server_name = 'localhost'

providers_credentials = {
    'Facebook': {
        'id': '***** your id here *****',
        'secret': '***** your secret here *****'
    },
    'Twitter': {
        'id': '***** your id here *****',
        'secret': '***** your secret here *****'
    },
    'Google': {
        'id': '***** your id here *****',
        'secret': '***** your secret here *****'
    }
}
