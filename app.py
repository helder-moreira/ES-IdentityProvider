from config import *
from flask import Flask,redirect, url_for, render_template, request, session, jsonify, flash
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager, UserMixin, login_user, logout_user, current_user, login_required
from oauth import OAuthSignIn
from werkzeug import secure_filename
from werkzeug.security import gen_salt
from flask_oauthlib.provider import OAuth2Provider
from datetime import datetime, timedelta
import md5, uuid
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
import string
from os import urandom, path
from random import randint

app = Flask(__name__)
if server_port != 80:
    app.config['SERVER_NAME'] = server_name+':'+str(server_port)
else:
    app.config['SERVER_NAME'] = server_name
app.config['SECRET_KEY'] = 'onetapmeal'
app.secret_key = 'onetapmeal'
app.config["PERMANENT_SESSION_LIFETIME"] = timedelta(seconds=session_time)
app.config["OAUTH2_PROVIDER_TOKEN_EXPIRES_IN"] = token_time
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['OAUTH_CREDENTIALS'] = providers_credentials
app.config['UPLOAD_FOLDER'] = os.path.join(APP_ROOT, UPLOAD_FOLDER[1:])
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg', 'gif'])
app.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024

db = SQLAlchemy(app)
oauth = OAuth2Provider(app)
lm = LoginManager(app)
lm.login_view = 'index'

api_description = { '/api/signup': {'Info':'Signup for local account',
                                    'Required':'username,password,email,phone',
                                    'Optional':'image_link',
                                    'Returns':'result and message'},
                    '/api/signin': {'Info':'Signin using local account',
                                    'Required':'username,password',
                                    'Returns':'result,message and token if sucess'},
                    '/api/signin/<provider>':{'Info':'Signin using social account',
                                    'Supported Providers (case sensitive)':'Facebook, Twitter, Google',
                                    'Returns':'link, callback_uuid or error if error occured'},
                    '/api/gettoken/<callback_uuid>':{'Info':'Exchange callback_uuid for token',
                                    'Required':'<callback_uuid>',
                                    'Returns':'result, message and token if sucess'},
                    '/api/login_app_password':{'Info':'Login with an app password',
                                    'Required':'username,password',
                                    'Returns':'result and message'},
                    '/api/getuser':{'Info':'Get user id and username',
                                    'Required':'token or phone',
                                    'Returns':'result, message, username and user_id if sucess'}
                    }

AccessCodes = dict()

def clearAccessCodes():
    for code in AccessCodes.copy():
        if datetime.utcnow() > AccessCodes[code][1]:
            del(AccessCodes[code])

def hash_pass(password):
    salted_password = password + app.secret_key
    return md5.new(salted_password).hexdigest()

def generate_temp_password():
    chars = string.ascii_letters + string.digits
    return "".join([chars[ord(c) % len(chars)] for c in urandom(temp_passwords_length)])

class AppPassword(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    password_hash = password_hash = db.Column(db.String(64), nullable=True)
    expires = db.Column(db.DateTime)
    user_id = db.Column(db.ForeignKey('user.id'))
    user = db.relationship('User')

def removeExpiredPasswords():
    passwords = AppPassword.query.filter(AppPassword.expires < datetime.utcnow()).all()
    for password in passwords:
        db.session.delete(password)
    db.session.commit()

class SocialAccount(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    provider = db.Column(db.String(64), nullable=False)
    social_id = db.Column(db.String(64), nullable=False, unique=True)
    user_id = db.Column(db.ForeignKey('user.id'))
    user = db.relationship('User')

class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), nullable=False, unique=True)
    password_hash = db.Column(db.String(64), nullable=True)
    email = db.Column(db.String(64), nullable=True)
    phone = db.Column(db.String(12), nullable=True, unique=True)
    image = db.Column(db.String(), nullable=True)

    def generate_auth_token(self):
        s = Serializer(app.config['SECRET_KEY'], expires_in = token_time)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = User.query.get(data['id'])
        return user

class Client(db.Model):
    name = db.Column(db.String(40))
    client_id = db.Column(db.String(40), primary_key=True)
    client_secret = db.Column(db.String(55), nullable=False)
    user_id = db.Column(db.ForeignKey('user.id'))
    user = db.relationship('User')
    _redirect_uris = db.Column(db.Text)
    _default_scopes = db.Column(db.Text)

    @property
    def client_type(self):
        return 'public'

    @property
    def redirect_uris(self):
        if self._redirect_uris:
            return self._redirect_uris.split()
        return []

    @property
    def default_redirect_uri(self):
        return self.redirect_uris[0]

    @property
    def default_scopes(self):
        if self._default_scopes:
            return self._default_scopes.split()
        return []

class Grant(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    user = db.relationship('User')

    client_id = db.Column(
        db.String(40), db.ForeignKey('client.client_id'),
        nullable=False,
    )
    client = db.relationship('Client')

    code = db.Column(db.String(255), index=True, nullable=False)

    redirect_uri = db.Column(db.String(255))
    expires = db.Column(db.DateTime)

    _scopes = db.Column(db.Text)

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []


class Token(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(
        db.String(40), db.ForeignKey('client.client_id'),
        nullable=False,
    )
    client = db.relationship('Client')

    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id')
    )
    user = db.relationship('User')

    token_type = db.Column(db.String(40))
    access_token = db.Column(db.String(255), unique=True)
    refresh_token = db.Column(db.String(255), unique=True)
    expires = db.Column(db.DateTime)
    _scopes = db.Column(db.Text)

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    @property
    def scopes(self):
        if self._scopes:
            return self._scopes.split()
        return []

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@oauth.clientgetter
def load_client(client_id):
    return Client.query.filter_by(client_id=client_id).first()


@oauth.grantgetter
def load_grant(client_id, code):
    return Grant.query.filter_by(client_id=client_id, code=code).first()


@oauth.grantsetter
def save_grant(client_id, code, request, *args, **kwargs):
    expires = datetime.utcnow() + timedelta(seconds=grant_time)
    grant = Grant(
        client_id=client_id,
        code=code['code'],
        redirect_uri=request.redirect_uri,
        _scopes=' '.join(request.scopes),
        user=current_user,
        expires=expires
    )
    db.session.add(grant)
    db.session.commit()
    return grant


@oauth.tokengetter
def load_token(access_token=None, refresh_token=None):
    if access_token:
        return Token.query.filter_by(access_token=access_token).first()
    elif refresh_token:
        return Token.query.filter_by(refresh_token=refresh_token).first()


@oauth.tokensetter
def save_token(token, request, *args, **kwargs):
    toks = Token.query.filter_by(
        client_id=request.client.client_id,
        user_id=request.user.id
    )
    # make sure that every client has only one token connected to a user
    for t in toks:
        db.session.delete(t)

    expires_in = token.pop('expires_in')
    expires = datetime.utcnow() + timedelta(seconds=expires_in)

    tok = Token(
        access_token=token['access_token'],
        refresh_token=token['refresh_token'],
        token_type=token['token_type'],
        _scopes=token['scope'],
        expires=expires,
        client_id=request.client.client_id,
        user_id=request.user.id,
    )
    db.session.add(tok)
    db.session.commit()
    return tok

@app.route('/oauth/token', methods=['POST'])
@oauth.token_handler
def access_token():
    return None

@app.route('/oauth/authorize', methods=['GET', 'POST'])
@login_required
@oauth.authorize_handler
def authorize(*args, **kwargs):
    if current_user.is_anonymous:
        return redirect(url_for('index'))
    if request.method == 'GET':
        client_id = kwargs.get('client_id')
        scopes = kwargs.get('scopes')[0]
        grant = Grant.query.filter(Grant.user_id==current_user.id)\
                .filter(Grant.client_id==client_id)\
                .filter(Grant._scopes==scopes)\
                .filter(Grant.expires > datetime.utcnow()).first()
        if grant:
            return True
        client = Client.query.filter_by(client_id=client_id).first()
        kwargs['client'] = client
        return render_template('authorize.html', **kwargs)

    confirm = request.form.get('confirm', 'no')
    return confirm == 'yes'


@app.route('/',methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        username = request.form.get('username')
        error = False
        if username=='':
            error = True
            flash('username required')
        password = request.form.get('password')
        if password=='':
            error = True
            flash('password required')
        password_h = hash_pass(password)
        if error:
            return redirect(url_for('index'))
        user = User.query.filter_by(username=username,password_hash=password_h).first()
        if not user:
            flash('Invalid credentials')
            return render_template('index.html')
        login_user(user, remember=(request.form.getlist('rememberme') != []))
        return redirect(url_for('index',result='success'))
    redirect_uri = request.args.get('next', None)
    if redirect_uri:
        session['redirect_uri'] = redirect_uri
    else:
        redirect_uri = session.pop('redirect_uri', None)
    if (not current_user.is_anonymous) and redirect_uri:
        return redirect(redirect_uri+'?access_token='+current_user.generate_auth_token())
    return render_template('index.html')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/profile',methods=['GET', 'POST'])
@app.route('/profile/<update_password>',methods=['GET', 'POST'])
def profile(update_password=None):
    if current_user.is_anonymous:
        return redirect(url_for('index'))
    password_update = False
    details_update = False
    if request.method == 'POST':
        if update_password:
            password_old = request.form.get('password_old')
            password_new = request.form.get('password_new')
            password_new_r = request.form.get('password_new_r')
            password_old_h = None
            if password_old != '':
                password_old_h = hash_pass(password_old)
            if password_old_h != current_user.password_hash:
                flash('Invalid Current Password')
            elif password_new != password_new_r:
                flash('Passwords do not match')
            else:
                current_user.password_hash = hash_pass(password_new)
                password_update = True
                db.session.commit()
        else:
            file = request.files['image']
            error = False
            filename = None
            filepath = None
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                filepath = path.join(app.config['UPLOAD_FOLDER'], filename)
            email = request.form.get('email')
            phone = request.form.get('phone')
            if not error:
                current_user.email = email
                current_user.phone = phone
                if filepath:
                    current_user.image = path.join(UPLOAD_FOLDER, filename)
                    file.save(filepath)
                db.session.commit()
                details_update = True
    providers = SocialAccount.query.filter_by(user=current_user).all()
    providers = [ x.provider for x in providers]
    return render_template('profile.html', password_update=password_update,details_update=details_update, providers=providers)

@app.route('/new_app',methods=['GET', 'POST'])
def new_app():
    if current_user.is_anonymous:
        return redirect(url_for('index'))
    if request.method == 'POST':
        app_name = request.form.get('app_name')
        error = False
        if app_name=='':
            error = True
            flash('App name cannot be empty')
        redirect_uri = request.form.get('redirect_uri')
        if redirect_uri=='':
            error = True
            flash('Redirect URI cannot be empty')
        if error:
            return redirect(url_for('new_app'))
        item = Client(
            name=app_name,
            client_id=gen_salt(40),
            client_secret=gen_salt(50),
            _redirect_uris=' '.join([redirect_uri]),
            _default_scopes='email phone user_id image',
            user_id=current_user.id,
        )
        db.session.add(item)
        db.session.commit()
        return render_template('created_app.html', client_id=item.client_id, client_secret=item.client_secret)
    return render_template('new_app.html', apps = Client.query.filter_by(user=current_user).all())

@app.route('/new_app_password',methods=['GET', 'POST'])
def new_app_password():
    removeExpiredPasswords()
    if current_user.is_anonymous:
        return redirect(url_for('index'))
    if request.method == 'POST':
        app_name = request.form.get('app_name')
        error = False
        if app_name=='':
            error = True
            flash('App name cannot be empty')
        password = generate_temp_password()
        if error:
            return redirect(url_for('new_app_password'))
        item = AppPassword(
            name=app_name,
            password_hash = hash_pass(password),
            user_id=current_user.id,
            expires = datetime.utcnow() + timedelta(seconds=app_passwords_time),
        )
        db.session.add(item)
        db.session.commit()
        return render_template('created_app_password.html', app_name=app_name, password=password)
    return render_template('new_app_password.html', apps = AppPassword.query.filter_by(user=current_user).all())

@app.route('/deleteAppPassword/<app_id>')
def deleteAppPassword(app_id):
    if current_user.is_anonymous:
        return redirect(url_for('index'))
    app = AppPassword.query.filter_by(user=current_user, id=app_id).first()
    db.session.delete(app)
    db.session.commit()
    return render_template('new_app_password.html', apps = AppPassword.query.filter_by(user=current_user).all())

@app.route('/deleteApp/<app_id>')
def deleteApp(app_id):
    if current_user.is_anonymous:
        return redirect(url_for('index'))
    app = Client.query.filter_by(user=current_user, client_id=app_id).first()
    db.session.delete(app)
    db.session.commit()
    return render_template('new_app.html', apps = Client.query.filter_by(user=current_user).all())

@app.route('/signup',methods=['GET', 'POST'])
def signup():
    if not current_user.is_anonymous:
        return redirect(url_for('index'))
    if request.method == 'POST':
        username = request.form.get('username')
        error = False
        if username=='':
            error = True
            flash('Username cannot be empty')
        if User.query.filter_by(username=username).first():
            error = True
            flash('Username already exists')
        password = request.form.get('password')
        if password=='':
            error = True
            flash('Password cannot be empty')
        password_h = hash_pass(password)
        password_h_r = hash_pass(request.form.get('password_r'))
        if password_h!=password_h_r:
            error = True
            flash('Passwords do not match')
        email = request.form.get('email')
        if email=='':
            error = True
            flash('Email cannot be empty')
        phone = request.form.get('phone')
        if phone=='':
            error = True
            flash('Phone cannot be empty')
        if User.query.filter_by(phone=phone).first():
            error = True
            flash('Phone number already exists in database')
        if not phone.isdigit():
            error = True
            flash('Phone can only contain digits')
        file = request.files['image']
        filename = None
        filepath = None
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = path.join(app.config['UPLOAD_FOLDER'], filename)
        if error:
            return redirect(url_for('signup'))
        image = None
        if filepath:
            image = path.join(UPLOAD_FOLDER, filename)
            file.save(filepath)
        user = User(username=username,password_hash=password_h,email=email,phone=phone,image=image)
        db.session.add(user)
        db.session.commit()
        login_user(user, remember=False)
        return redirect(url_for('index',result='success'))
    return render_template('signup.html')

@app.route('/signout')
def signout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    oauth = OAuthSignIn.get_provider(provider)
    return redirect(oauth.authorize())


@app.route('/callback/<provider>')
def oauth_callback(provider):
    clearAccessCodes()
    oauth = OAuthSignIn.get_provider(provider)
    (social_id, username, email, image), UUID = oauth.callback()
    if social_id is None:
        flash('Authentication failed.')
        return redirect(url_for('index'))
    social = SocialAccount.query.filter_by(social_id=social_id).first()
    if not social:
        if current_user.is_anonymous:
            if UUID:
                session['UUID'] = UUID
            session['provider'] = provider
            session['social_id'] = social_id
            session['username'] = username
            session['email'] = email
            session['image'] = image
            return render_template('post_provider.html')
        social = SocialAccount(provider=provider, social_id=social_id, user_id=current_user.id)
        db.session.add(social)
        db.session.commit()
    user = User.query.filter_by(id=social.user_id).first()
    if not user.image:
        user.image = image
        db.session.commit()
    if UUID in AccessCodes.keys():
        AccessCodes[UUID][0] = user.id
    login_user(user, remember_me)
    return redirect(url_for('index',result='success'))

@app.route('/callback/post_provider', methods=['GET', 'POST'])
def post_provider():
    if request.method == 'POST':
        phone = request.form.get('phone')
        if User.query.filter_by(phone=phone).first():
            flash('Phone number already exists in database')
            return render_template('post_provider.html')
        provider = session['provider']
        session.pop('provider', None)
        social_id = session['social_id']
        session.pop('social_id', None)
        username = session['username']
        session.pop('username', None)
        email = session['email']
        session.pop('email', None)
        image = session['image']
        session.pop('image', None)
        UUID = None
        if 'UUID' in session:
            UUID = session['UUID']
            session.pop('UUID', None)
        if phone and phone.isdigit() and social_id and username and email:
            while User.query.filter_by(username=username).first():
                username = username + '_' + str(randint(1,1000))
            user = User(username=username, password_hash=None, email=email, phone=phone, image=image)
            db.session.add(user)
            db.session.commit()
            social = SocialAccount(provider=provider, social_id=social_id, user_id=user.id)
            db.session.add(social)
            db.session.commit()
            if UUID in AccessCodes.keys():
                AccessCodes[UUID][0] = user.id
            login_user(user, remember_me)
            return redirect(url_for('index',result='success'))
        flash('Invalid phone number')
    return render_template('post_provider.html')


@app.route('/api', methods=['GET'])
def api():
    return jsonify(api_description)

@app.route('/api/signup', methods=['POST'])
def api_signup():
    required = ('username','password','email','phone')
    data = request.get_json(force=True)
    if set(required).issubset(data):
        username = data['username']
        if User.query.filter_by(username=username).first():
            return jsonify(result='error',message='username already exists')
        password = data['password']
        email = data['email']
        phone = data['phone']
        if not phone.isdigit():
            return jsonify(result='error',message='Phone number can only contain digits')
        if 'image' in data:
            image = data['image']
        else:
            image = None
        if username and password and email and phone:
            user = User(username=username,password_hash=hash_pass(password),email=email,phone=phone,image=image)
            db.session.add(user)
            db.session.commit()
            return jsonify(result='success',message='User created sucessfuly')
        return jsonify(result='error',message='User could not be created')
    return jsonify(result='error',message='Not all required filds were sent')

@app.route('/api/signin', methods=['POST'])
def api_signin():
    required = ('username','password')
    data = request.get_json(force=True)
    if set(required).issubset(data):
        username = data['username']
        password = data['password']
        password_h = hash_pass(password)
        user = User.query.filter_by(username=username,password_hash=password_h).first()
        if not user:
            return jsonify(result='error',message='Invalid login credentials')
        login_user(user)
        return jsonify(result='success',token=user.generate_auth_token(), message='Success')
    return jsonify(result='error',message='Not all required filds were sent')

@app.route('/api/login_app_password', methods=['POST'])
def api_login_app_password():
    required = ('username','password')
    data = request.get_json(force=True)
    if set(required).issubset(data):
        username = data['username']
        password = data['password']
        password_h = hash_pass(password)
        user = User.query.filter_by(username=username).first()
        if not user:
            return jsonify(result='error',message='Invalid login credentials')
        passwords = AppPassword.query.filter_by(user=user).all()
        if not passwords:
            return jsonify(result='error',message='Invalid login credentials')
        for p in passwords:
            if p.password_hash == password_h:
                login_user(user)
                return jsonify(result='success', message='Success')
        return jsonify(result='error',message='Invalid login credentials')
    return jsonify(result='error',message='Not all required filds were sent')

@app.route('/api/signin/<provider>')
def api_oauth_authorize(provider):
    clearAccessCodes()
    if provider not in [key for key in app.config['OAUTH_CREDENTIALS']]:
        return jsonify(error='Provider not Supported')
    oauth = OAuthSignIn.get_provider(provider)
    callback_uuid = str(uuid.uuid4())
    expires = datetime.utcnow() + timedelta(seconds=uuid_time)
    AccessCodes[callback_uuid] = [None, expires]
    return jsonify(authorize_url=oauth.authorize(callback_uuid),callback_uuid=callback_uuid)

@app.route('/api/gettoken/<UUID>')
def api_gettoken(UUID):
    if UUID in AccessCodes.keys():
        user_id = AccessCodes.pop(UUID)[0]
        user = User.query.get(int(user_id))
        if user:
            return jsonify(result='success',token=user.generate_auth_token(), message='Success')
    return jsonify(result='error',message='Invalid or expired uuid')

@app.route('/api/getuser', methods=['POST'])
def api_getuser():
    data = request.get_json(force=True)
    if 'token' in data:
        token = data['token']
        user = User.verify_auth_token(token)
        if not user:
            return jsonify(result='error',message='Invalid or expired token')
        return jsonify(result='success',user_id=user.id, username=user.username, message='Success')
    if 'phone' in data:
        phone = data['phone']
        user = User.query.filter_by(phone=phone).first()
        if not user:
            return jsonify(result='error',message='Invalid or non existing phone')
        return jsonify(result='success',user_id=user.id, username=user.username, message='Success')
    return jsonify(result='error',message='Required filds not sent')

if __name__ == '__main__':
    db.create_all()
    app.run(debug=debug,host=server_name, port=server_port)
