from flask import Flask, url_for, session, request, jsonify
from flask_oauthlib.client import OAuth, OAuthException


CLIENT_ID = '5SrTm3dYyoRTi89ALR8LGQXj1wJHUHDLiqX14DM0'
CLIENT_SECRET = 'B3sHCPVJOUzWVMPEHon8JJWlsNaogWs38tAzcgQDefLC8Td0VL'


app = Flask(__name__)
app.debug = True
app.secret_key = 'onetapmeal'
oauth = OAuth(app)

remote = oauth.remote_app(
    'remote',
    consumer_key=CLIENT_ID,
    consumer_secret=CLIENT_SECRET,
    request_token_params={'scope': 'email phone image user_id'},
    base_url='http://localhost:5000/api/',
    request_token_url=None,
    access_token_url='http://localhost:5000/oauth/token',
    authorize_url='http://localhost:5000/oauth/authorize',
)


@app.route('/')
def index():
    auth = request.headers.get('Authorization')
    if auth:
        session['remote_oauth'] = (auth, '')
    if 'remote_oauth' in session:
        resp = remote.get('me')
        try:
            ret = jsonify(resp.data)
            return ret
        except ValueError:
            print("exception")
    next_url = request.args.get('next') or request.referrer or None
    return remote.authorize(
        callback=url_for('authorized', next=next_url, _external=True)
    )


@app.route('/authorized')
def authorized():
    resp = remote.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args['error_reason'],
            request.args['error_description']
        )
    if isinstance(resp, OAuthException):
        return jsonify(error='Could not get access token')
    else:
        session['remote_oauth'] = (resp['access_token'], '')
        return jsonify(oauth_token=resp['access_token'])


@remote.tokengetter
def get_oauth_token():
    return session.get('remote_oauth')


if __name__ == '__main__':
    import os
    os.environ['DEBUG'] = 'true'
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = 'true'
    app.run(host='localhost', port=8000)
