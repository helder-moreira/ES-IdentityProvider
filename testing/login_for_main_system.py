from flask import Flask, session, redirect, request, jsonify
import requests, json
app = Flask(__name__)

app.secret_key = '1tapmeal'
authentication_server = 'http://idp.moreirahelder.com'
token_validation_server = 'http://idp.moreirahelder.com/api/getuser'
my_server = 'http://127.0.0.1:3000/auth_callback'

def validate_token(token):
	if not token:
		return None
	headers = {'Content-type': 'application/json'}
	data = {'token': token} 
	response = requests.post(token_validation_server, json.dumps(data), headers=headers)
	data = json.loads(response.text)
	if data['result']:
		return (data['user_id'], data['username'])
	return None

@app.route("/auth_callback")
def auth_callback():
	token = request.args.get('access_token', None)
	if token:
		session['access_token'] = token
	return redirect('/')

@app.route("/")
def hello():
	token = session.get('access_token', None)
	user = validate_token(token)
	if user:
		return 'Authenticated: '+user[1]
	return redirect(authentication_server+"?next="+my_server)

if __name__ == "__main__":
	app.run(port=3000)